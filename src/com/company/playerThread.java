package com.company;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.advanced.AdvancedPlayer;
import javazoom.jl.player.advanced.PlaybackEvent;
import javazoom.jl.player.advanced.PlaybackListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class playerThread extends Thread{
    private File mp3files;
    protected AdvancedPlayer player;
    private int pausedMoment=0;
    private volatile boolean running=true;
    private int pos=0;

    public playerThread(File mp3file,int pos){
        this.mp3file=mp3file;
        this.pos=pos;
    }

    @Override
    public void run(){
        while(running){
            try {
                FileInputStream ar = new FileInputStream(mp3file.getAbsoluteFile().getAbsolutePath());
                this.player=new AdvancedPlayer(ar);
                System.out.println("Now playing: "+mp3file.getName());
                player.play(pos,Integer.MAX_VALUE);
            }catch (JavaLayerException e) {
                e.printStackTrace();
            }catch (FileNotFoundException ex){
                ex.printStackTrace();
            }
        }
    }

    public void pausePlay(){
        player.setPlayBackListener(new PlaybackListener() {
            @Override
            public void playbackFinished(PlaybackEvent playbackEvent) {
                pausedMoment=playbackEvent.getFrame();
            }
        });
        player.stop();
        running=false;
        System.out.println("Paused playing of "+mp3file.getName());
    }

    public void setRunning(){
        running=true;
    }

    public void setPos(int pos){
        this.pos=pos;
    }

    public void stopPlay(){
        running=false;
        player.close();
        System.out.println("Stopped playing of "+mp3file.getName());
    }

    public int getPausedMoment(){
        return pausedMoment;
    }
}
