# jpmp3

jpmp3 is a music player designed for CLIs, made in Java with JLayer.

jpmp3 is cross-platform and works on Windows and GNU/Linux, not sure about MacOS tho.

# Current status

Right now jpmp3 is on it's early stages (and I wouldn't expect it to leave them) and does not have almost any functionality built in.

I plan to add working pause/resume(right now it resumes 20 seconds ahead), ID3 tag reading and all that comes with that, and making it more user friendly, although let's not lie, if you are here you can find your way around.

I'm going to add a config file so you don't have to input the path everytime.

# Execution

To run, type on your preferred terminal emulator (or cmd on windows):

java -jar /path/to/jar (optional)/path/to/music

My recommendation if you are on GNU/Linux is to add it as a keyboard shortcut.
If no path to music is received as argument it will ask for it upon execution.

# Instructions

To build from source you'll need JLayer, get it here: http://www.javazoom.net/javalayer/sources.html
Upon development time and when ID3 tag reading gets implemented, JID3 will be also needed.

If you don't want to build from source just download jpmp3.jar and do the above steps.

# License

MIT License

Copyright (c) 2018 Adrián Pérez del Pulgar García

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
